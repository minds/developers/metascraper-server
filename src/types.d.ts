type ImageData = any;
type Element = any;
type Event = any;
type Blob = any;
type ProgressEvent = any;
type File = any;
type EventTarget = any;
type MediaStream = any;
type SubtleCrypto = any;
type CryptoKey = any;
type KeyAlgorithm = any;

declare module 'browserless';
declare module 'html-get';
declare module '@metascraper/helpers';
