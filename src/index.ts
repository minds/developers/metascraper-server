/**
 * Metascraper Server for parsing metadata from URLs.
 * See README for setup instructions.
 * @author Minds - Ben Hayward
 */
import express from 'express';
import cors from 'cors';
import bodyParser from 'body-parser';
import swaggerUI from 'swagger-ui-express';
import swaggerJsDoc from 'swagger-jsdoc';
import { router as scrapeRoutes } from './routes/scrape';

const app = express();
const port = process.env.PORT || 3334;
const swaggerSpecs: Object = swaggerJsDoc({
  definition: {
    openapi: '3.0.1',
    info: {
      openapi: '3.0.1',
      title: 'Minds Metascraper Server',
      version: '1.0.0',
      info: {
        title: 'Minds Metascraper Server',
        version: '1.0.0'
      }
    }
  },
  apis: ['./src/routes/*.ts', './src/routes/skale/*.ts']
});

app.use(bodyParser.json()); // support json encoded bodies.
app.use(bodyParser.urlencoded({ extended: true })); // support encoded bodies.
app.use(express.json()); // any request coming in, transfer all body into JSON.
app.use(cors()); // allow cross origin from client localhost.
app.use('/docs', swaggerUI.serve, swaggerUI.setup(swaggerSpecs)); // docs route.
app.use('/scrape', scrapeRoutes); // scrape route.

/**
 * Health check endpoint
 * @returns { Object } status of 200, readyState of 1.
 */
app.get('/', (req, res) => {
  res.send({
    status: 200,
    readyState: 1
  });
});

/**
 * Sets app to listen on set port.
 */
const server = app.listen(port, () => {
  console.log('Server running on port ' + port);
});

/**
 * Close server.
 */
export const closeServer = () => {
  server.close();
};

export default app;
