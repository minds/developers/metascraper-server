/**
 * Scrape routes - the core metadata scraper.
 */
import express, { Router } from 'express';
import { Metadata } from 'metascraper';
import { buildMetascraper } from '../common/buildMetascraper';
import { fetchData } from '../common/fetchData';
import { formatUrl } from '../common/formatUrl';
import { isValidMetadata } from '../common/isValidMetadata';

const router: Router = express.Router();
const metascraper = buildMetascraper();

/**
 * @swagger
 * /scrape:
 *   get:
 *     summary: Scrapes URL for rich-embed data
 *     tags: [scrape]
 *     produces:
 *       - application/json
 *     consumes:
 *       - application/json
 *     parameters:
 *       - name: targetUrl
 *         in: query
 *         required: true
 *         type: string
 *         description: URL that you want to scrape for metadata.
 *     responses:
 *       200:
 *         description: Success, returns metadata for URL.
 *       400:
 *         description: Missing target URL.
 *       500:
 *         description: Internal server error.
 */
router.get('/', async (req: express.Request, res: express.Response) => {
  let targetUrl: string = req.query.targetUrl as string;

  if (!targetUrl) {
    return res.json({
      status: 400,
      message: 'Missing targetUrl'
    });
  }

  targetUrl = formatUrl(targetUrl);
  console.log('Scraping URL:', targetUrl);

  try {
    const result: any = await fetchData(targetUrl);

    const html = result && result.html ? result.html : false;
    const url = result && result.url ? result.url : false;

    if (!html || !url) {
      throw new Error('Unable to fetch all data.');
    }

    const metadata: Metadata = await metascraper({ html, url });

    if (!isValidMetadata(metadata)) {
      console.error('Invalid metadata returned:', metadata);
      throw new Error('Unable to parse metadata from target URL');
    }

    return res.status(result.statusCode).json({
      status: result.statusCode,
      data: metadata
    });
  } catch (e: any) {
    console.error(e);
    return res.status(500).json({
      status: 500,
      message: e.message ?? 'An error has occurred'
    });
  }
});

export { router };
