import { Metadata } from 'metascraper';

/**
 * Whether metadata is valid.
 * @returns { boolean } - true if metadata is valid.
 */
export const isValidMetadata = (metadata: Metadata): boolean => {
  return (
    metadata &&
    (metadata['image'] ||
      (metadata as any)['iframe'] ||
      metadata['description'])
  );
};
