import metascraper from 'metascraper';
import metascraperAuthor from 'metascraper-author';
import metascraperDate from 'metascraper-date';
import metascraperDescription from 'metascraper-description';
import metascraperImage from 'metascraper-image';
import metascraperLogo from 'metascraper-logo';
import metascraperTitle from 'metascraper-title';
import metascraperUrl from 'metascraper-url';
import metascraperIframe from 'metascraper-iframe';
import metascraperAmazon from 'metascraper-amazon';
import metascraperTwitter from 'metascraper-twitter';
import metascraperPublisher from 'metascraper-publisher';

/**
 * Returns built Metascraper with all Rules applied.
 */
export const buildMetascraper = () => {
  return metascraper([
    metascraperAmazon(),
    metascraperAuthor(),
    metascraperDate({
      datePublished: true
    }),
    metascraperDescription(),
    metascraperImage(),
    metascraperLogo(),
    metascraperTitle(),
    metascraperTwitter(),
    metascraperUrl(),
    metascraperIframe(),
    metascraperPublisher()
  ]);
};
