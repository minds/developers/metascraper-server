import createBrowserless from 'browserless';
import getHTML from 'html-get';
import { parseUrl } from '@metascraper/helpers';

// list of domains to exclude from pre-rendering.
const preRenderExclusions: string[] = [
  'youtube',
  'youtu',
  'google',
  'wordpress',
  'apple',
  'wikipedia',
  'microsoft',
  'blogspot',
  'vimeo',
  'github',
  'nytimes',
  'bbc',
  'imdb',
  'theguardian',
  'slideshare',
  'telegraph',
  'pinterest',
  'soundcloud',
  'huffingtonpost',
  'eventbrite',
  'spotify',
  'zoom',
  'techcrunch',
  'yelp',
  'engadget',
  'theverge',
  'reddit',
  'stackoverflow',
  'flickr',
  'csdn',
  'digg',
  'etsy',
  'ghost',
  'giphy',
  'imgur',
  'meetup',
  'producthunt',
  'sourceforge',
  'tumblr',
  'ycombinator',
  'twitter'
  // 'tabletmag'
];

/**
 * Fetch data using Browserless.
 * @param { string } targetUrl - target URL to get.
 * @returns { Promise<any> } - the result.
 */
export const fetchData = async (targetUrl: string): Promise<any> => {
  try {
    const browserlessFactory = createBrowserless({
      timeout: -1
    });

    // Kill the process when Node.js exit
    process.on('exit', browserlessFactory.close);

    // create a browser context inside Chromium process
    const browserContext = await browserlessFactory.createContext();
    const getBrowserless = () => browserContext;
    const result = await getHTML(targetUrl, {
      getBrowserless,
      headers: {
        'user-agent': 'facebookexternalhit/1.1'
      },
      getMode: (url: string, { prerender }: any) => {
        const domain: string = parseUrl(url).domainWithoutSuffix;
        return preRenderExclusions.includes(domain) ? 'fetch' : 'prerender';
      }
    });

    // close the browser context after it's used
    const browser = await getBrowserless();
    await browser.destroyContext();
    await browserlessFactory.close();

    return result;
  } catch (e) {
    console.error(e);
    return null;
  }
};
