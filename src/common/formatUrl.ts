/**
 * Format a URL - prepending https if it is not already present.
 * @param { string } url - URL to format.
 * @returns { string } - adjusted (if neccesary) URL.
 */
export const formatUrl = (url: string): string => {
  return url.startsWith('http') ? url : `https://${url}`;
};
