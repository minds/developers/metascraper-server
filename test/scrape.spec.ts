import app, { closeServer } from "../src/index";
import supertest from 'supertest';
import { isValidMetadata } from "../src/common/isValidMetadata";

describe("Scrape Endpoint", () => {
  afterAll(() => closeServer());

  it("should check healthcheck endpoint", (done) => {
    supertest(app)
      .get('/')
      .expect(200)
      .field('readyState', 1)
      .end(done);
  });

  it("should check Minds homepage", (done) => {
    supertest(app)
      .get("/scrape?targetUrl=https://www.minds.com/")
      .expect(200)
      .end((err, res) => {
        if (err) return done(err);
        expect(res.body.data).toEqual(
          expect.objectContaining({
            title : 'Minds',
            image: 'https://www.minds.com/static/en/assets/og-images/default-v3.png',
            description: 'Elevate the global conversation through Internet freedom. Speak freely, protect your privacy, earn crypto, and take back control of your social media',
            url: 'https://www.minds.com/',            
          })
        );
        done();
      });
  });

  it("should check Minds plus marketing page", (done) => {
    supertest(app)
      .get("/scrape?targetUrl=https://www.minds.com/plus")
      .expect(200)
      .end((err, res) => {
        if (err) return done(err);
        expect(res.body.data).toEqual(
          expect.objectContaining({
            title : 'Minds+',
            image: 'https://www.minds.com/static/en/assets/og-images/plus-v3.png',
            description: 'Support Minds and unlock features such as hiding ads, accessing exclusive content, receiving a badge and verifying your channel.',
            url: 'https://www.minds.com/plus',            
          })
        );
        done();
      });
  });

  it("should check Minds token marketing page", (done) => {
    supertest(app)
      .get("/scrape?targetUrl=https://www.minds.com/token")
      .expect(200)
      .end((err, res) => {
        if (err) return done(err);
        expect(res.body.data).toEqual(
          expect.objectContaining({
            title : 'Tokens',
            image: 'https://www.minds.com/static/en/assets/og-images/tokens-v3.png',
            description: 'Buy Minds tokens to expand your reach, support your favorite channels, and unlock premium features on Minds.',
            url: 'https://www.minds.com/token',            
          })
        );
        done();
      });
  });

  it("should check Minds rewards marketing page", (done) => {
    supertest(app)
      .get("/scrape?targetUrl=https://www.minds.com/rewards")
      .expect(200)
      .end((err, res) => {
        if (err) return done(err);
        expect(res.body.data).toEqual(
          expect.objectContaining({
            title : 'Minds Rewards',
            image: 'https://www.minds.com/static/en/assets/og-images/rewards-v3.png',
            description: 'Earn tokens for your contributions to the network',
            url: 'https://www.minds.com/rewards',            
          })
        );
        done();
      });
  });

  it("should check Minds channel", (done) => {
    supertest(app)
      .get("/scrape?targetUrl=https://www.minds.com/minds")
      .expect(200)
      .end((err, res) => {
        if (err) return done(err);
        expect(res.body.data).toEqual(
          expect.objectContaining({
            title : 'Minds (@minds)',
            description: expect.any(String),
            url: 'https://www.minds.com/minds/',            
          })
        );
        expect(res.body.data.image.startsWith('https://www.minds.com/icon/100000000000000519/')).toBe(true);
        done();
      });
  });

  it("should check Minds newsfeed text post", (done) => {
    supertest(app)
      .get("/scrape?targetUrl=https://www.minds.com/newsfeed/1386414206269526035")
      .expect(200)
      .end((err, res) => {
        if (err) return done(err);
        expect(res.body.data).toEqual(
          expect.objectContaining({
            title : 'We’ve reached the point in society where debate is considered extremist activity.',
            image: 'https://www.minds.com/static/en/assets/og-images/default-v3.png',
            description: '...ered extremist activity.\n. Subscribe to @minds on Minds',
            url: 'https://www.minds.com/newsfeed/1386414206269526035',            
          })
        );
        done();
      });
  });

  it('should check Minds image post', (done) => {
    supertest(app)
      .get("/scrape?targetUrl=https://www.minds.com/newsfeed/1323765857448366100")
      .expect(200)
      .end((err, res) => {
        if (err) return done(err);
        expect(res.body.data).toEqual(
          expect.objectContaining({
            title : "@minds’s post on Minds",
            image: 'https://cdn.minds.com/fs/v1/thumbnail/1323765857448366100/xlarge/',
            description: 'Subscribe to @minds on Minds',
            url: 'https://www.minds.com/newsfeed/1323765857448366100',              
          })
        );
        done();
      });
  });

  it('should check Minds video post', (done) => {
    supertest(app)
    .get("/scrape?targetUrl=https://www.minds.com/newsfeed/1175810454042136576")
    .expect(200)
    .end((err, res) => {
      if (err) return done(err);
      expect(res.body.data).toEqual(
        expect.objectContaining({
          title : '@minds’s post on Minds',
          image: 'https://cdn.minds.com/fs/v1/thumbnail/1175810454042136576/medium/',
          description: 'Subscribe to @minds on Minds',
          url: 'https://www.minds.com/newsfeed/1175810454042136576',            
        })
      );
      done();
    });
  });

  it('should check Minds blog post', (done) => {
    supertest(app)
      .get("/scrape?targetUrl=https://www.minds.com/minds/blog/your-guide-to-using-minds-1195126377804414976")
      .expect(200)
      .end((err, res) => {
        if (err) return done(err);
        expect(res.body.data).toEqual(
          expect.objectContaining({
            title : 'Your Guide to Using Minds',
            image: 'https://cdn.minds.com/fs/v1/banners/1195126377804414976/1615922007',
            description: 'Welcome to Minds, and thank you for being a pioneer of the free and open Internet!The purpose of this blog is to highlight the many features...',
            url: 'https://www.minds.com/minds/blog/your-guide-to-using-minds-1195126377804414976',            
          })
        );
        done();
      });
  });

  const validMetadataUrls = [
    'https://rumble.com/val8vm-how-to-use-rumble.html',
    'https://rumble.com/',
    'https://open.spotify.com/episode/0hhjNGhrSsIF65pf76KJm4',
    'https://open.spotify.com/show/4rOoJ6Egrf8K2IrywzwOMk',
    'https://www.spotify.com/',
    'https://www.amazon.com',
    'https://www.amazon.co.uk',
    'https://www.buzzfeed.com',
    'https://www.indiatimes.com/explainers/news/how-climate-change-is-killing-insects-and-impacting-the-food-chain-573455.html',
    'https://www.indiatimes.com',
    'https://finance.yahoo.com',
    'https://www.foxnews.com/media/5-things-stream-fox-nation-celebrate-america-this-july',
    'https://www.foxnews.com',
    'https://www.theguardian.com/technology/2021/jan/11/what-bitcoin-why-many-people-buy-cryptocurrency-financial-regulator',
    'https://www.theguardian.com/',
    'https://news.google.com',
    'https://edition.cnn.com/2022/04/01/success/cryptocurrencies-401ks/index.html',
    'https://www.msn.com/',
    'https://www.bbc.co.uk/news/business-42135963',
    'https://www.politico.com/story/2013/08/congress-starts-looking-into-bitcoin-095464',
    'https://politico.com',
    'https://www.mirror.co.uk/',
    'https://www.mirror.co.uk/tech/what-is-bitcoin-digital-currency-10409961',
    'https://people.com/',
    'https://people.com/human-interest/bitcoin-passed-50k-first-time-what-you-need-to-know/',
    'https://www.abc.net.au/btn/classroom/bitcoin/10530720',
    'https://www.abc.net.au/',
    'https://www.dailymail.co.uk/',
    'https://www.dailymail.co.uk/money/markets/article-10685091/Rishi-asks-Royal-Mint-create-non-fungible-token.html',
    'https://nypost.com/2022/06/29/r-e-m-s-mike-mills-lists-la-home-near-hollywood-sign-for-6-5m/',
    'https://www.nytimes.com/2022/06/30/business/stock-market-worst-start-50-years.html',
    'https://www.nytimes.com',
    'https://www.tumblr.com/explore/trending?source=homepage_explore',
    'https://twitter.com/Twitter/status/1509951255388504066',
    'https://www.reddit.com/r/space/comments/arer0k/i_took_nearly_50000_images_of_the_night_sky_to/',
    'https://soundcloud.com/ulvermrt/aphex-twin-heliosphan',
    'https://www.youtube.com/watch?v=2tOutF8B3f8',
    'https://twitter.com/twitter',
    'https://twitter.com/Twitter/status/1580661436132757506/photo/1',
    'https://twitter.com/Twitter/status/1580661436132757506',
    'https://twitter.com/minds/status/1542899346395521027',
    'https://twitter.com/minds/status/1616159460098981911'
  ];

  for(let url of validMetadataUrls) {
    it(`should check ${url}`, (done) => {
      supertest(app)
        .get(`/scrape?targetUrl=${url}`)
        .expect(200)
        .end((err, res) => {
          if (err) return done(err);
          expect(isValidMetadata(res.body.data)).toBeTruthy();
          done();
        });
    });
  }
});
