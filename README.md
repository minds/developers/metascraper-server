# Minds Metascraper Server

Minds Metascraper Server is a microservice for parsing metadata from URLs using [Metascraper](https://metascraper.js.org/#/).

## Installation

Ensure you are using node version v14.17.1 (lts/fernium). If you are using NVM you can simply run:

```bash
nvm use
```

Next install dependencies:

```bash
npm install
```

Finally, start the server

```bash
npm run start
```

## Usage

While running, you can view the docs page at [localhost:3334/docs](localhost:3334/docs);

To scrape a URL, hit the following with your URL.

[localhost:3334/scrape?targetUrl=https%3A%2F%2Fwww.minds.com](localhost:3334/scrape?targetUrl=https%3A%2F%2Fwww.minds.com).


## Contributing

For information on contributing, please see:

[https://developers.minds.com/docs/contributing/contributing/](https://developers.minds.com/docs/contributing/contributing/).

## License
[MIT](https://choosealicense.com/licenses/mit/)
