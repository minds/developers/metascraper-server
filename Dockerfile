FROM node:18-alpine as build

# Set workspace to app.
WORKDIR /app

# Update apk and add chromium.
RUN apk update && apk add chromium

# Copy various files.
COPY src/ ./src/
COPY tsconfig.json tsconfig.json
COPY package.json package.json
COPY package-lock.json package-lock.json

# Skip chromium download - we're installing outside of NPM.
ENV PUPPETEER_SKIP_CHROMIUM_DOWNLOAD=true

# Install and build
RUN npm i
RUN npm run build

# This is second stage of build - allowing us to cut out
# unused artifacts to optimize the image.
FROM node:18-alpine

# Set workdir to root Minds folder.
WORKDIR /var/www/Minds/

# Update and install chromium.
RUN apk update && apk add chromium

# Set executable path for puppeteer since we do not 
# have it installed via NPM.
ENV PUPPETEER_EXECUTABLE_PATH=/usr/bin/chromium-browser

# Copy node modules and dist folders from build stage.
COPY --from=build /app/node_modules/ ./node_modules/
COPY --from=build /app/dist/ ./dist/

# Add user so we don't need --no-sandbox.
RUN addgroup -S pptruser && adduser -S -G pptruser pptruser \
    && chown -R pptruser:pptruser /home/pptruser \
    && chown -R pptruser:pptruser .

# Run everything after as non-privileged user.
USER pptruser

# Start.
CMD node dist/
